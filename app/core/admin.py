from django.contrib import admin

from user.models import User, UserProfile
from district.models import District
from product.models import Product
from verification.models import Verification, VerificationAddress

admin.site.register(User)
admin.site.register(District)
admin.site.register(UserProfile)
admin.site.register(Product)

admin.site.register(Verification)
admin.site.register(VerificationAddress)
