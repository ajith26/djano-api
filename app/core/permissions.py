"""Custom Permission for SuperUser"""

from rest_framework.permissions import BasePermission


class IsSuperAdmin(BasePermission):

    def has_permission(self, request, view, **args):
        if request.user.is_superuser:
            return True
        else:
            return False
