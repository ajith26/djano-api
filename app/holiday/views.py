"""Holiday APIs"""

from rest_framework.viewsets import ModelViewSet
from .serializers import HolidaySerializer
from holiday.models import Holiday
from core.permissions import IsSuperAdmin


class HolidayViews(ModelViewSet):
    serializer_class = HolidaySerializer
    queryset = Holiday.objects.all()
    permission_classes = {IsSuperAdmin}
