"""Holiday apis"""

from django.urls import path, include
from holiday import views
from rest_framework import routers

router = routers.DefaultRouter()

router.register('holiday', views.HolidayViews)

urlpatterns = [
    path("", include(router.urls)),

]
