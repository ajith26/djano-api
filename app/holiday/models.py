"""HOLIDAY"""

from django.db import models


class Holiday(models.Model):
    holiday_date = models.DateField()
    description = models.TextField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
