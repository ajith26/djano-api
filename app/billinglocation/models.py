"""Billing Location Model """

from django.db import models


class BillingLocation(models.Model):
    name = models.CharField(max_length=100)
    short_code = models.CharField(max_length=5, null=True, blank=True)
    description = models.TextField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.short_code = self.short_code.upper()
        super(BillingLocation, self).save(*args, **kwargs)
