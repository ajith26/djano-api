"""Serializer for Billing Location"""

from rest_framework import serializers
from billinglocation.models import BillingLocation


class BillingLocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = BillingLocation
        fields = '__all__'

    def create(self, validated_data):
        return BillingLocation.objects.all().create(**validated_data)
