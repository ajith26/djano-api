from django.apps import AppConfig


class BillinglocationConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'billinglocation'
