"""Billing Location """

from django.urls import path, include
from billinglocation import views
from rest_framework import routers

router = routers.DefaultRouter()

router.register('billing-location', views.BillingLocationViews)

urlpatterns = [
    path("", include(router.urls)),

]
