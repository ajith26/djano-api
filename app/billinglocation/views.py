"""Billing Location APIs"""

from rest_framework.viewsets import ModelViewSet
from .serializers import BillingLocationSerializer
from core.permissions import IsSuperAdmin
from billinglocation.models import BillingLocation


class BillingLocationViews(ModelViewSet):
    serializer_class = BillingLocationSerializer
    permission_classes = {IsSuperAdmin}
    queryset = BillingLocation.objects.all()
