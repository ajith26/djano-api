"""District APIs"""

from rest_framework.viewsets import ModelViewSet
from .serializers import DistrictSerializer
from district.models import District
from core.permissions import IsSuperAdmin
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters


class DistrictViews(ModelViewSet):
    serializer_class = DistrictSerializer
    permission_classes = {IsSuperAdmin}
    queryset = District.objects.all()
    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    filterset_fields = ['created_at']
    # search_fields = ['name']
    filterset_fields = {
     'id': ['gte', 'lte', 'exact', 'gt', 'lt'],
     'name': ['exact'],
    }