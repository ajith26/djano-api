"""Serializer for District"""

from rest_framework import serializers
from district.models import District


class DistrictSerializer(serializers.ModelSerializer):
    class Meta:
        model = District
        fields = '__all__'

    def create(self, validated_data):
        return District.objects.all().create(**validated_data)


class AddrDistrictSerializer(serializers.ModelSerializer):
    class Meta:
        model = District
        fields = ['name']
        