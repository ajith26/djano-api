"""District apis"""

from django.urls import path, include
from district import views
from rest_framework import routers

router = routers.DefaultRouter()

router.register('district', views.DistrictViews)

urlpatterns = [
    path("", include(router.urls)),

]
