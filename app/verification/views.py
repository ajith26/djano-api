"""Verification Views"""

from rest_framework import viewsets
from verification.serializers import AddVerificationSerializer
from verification.models import Verification


class AddVerificationViews(viewsets.ModelViewSet):
    serializer_class = AddVerificationSerializer
    queryset = Verification.objects.all()

