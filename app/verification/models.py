"""Verification Model"""

from django.db import models
from user.models import UserProfile
from product.models import Product
from district.models import District


class Verification(models.Model):
    """Verification """
    vendor = models.ForeignKey(
        UserProfile,
        related_name="vendor",
        on_delete=models.CASCADE
        )
    product = models.ForeignKey(
        Product,
        related_name="product",
        on_delete=models.CASCADE
    )
    application_id = models.CharField(
        max_length=50,
        blank=True,
        null=True
    )
    applicant_type = models.CharField(max_length=10)

    customer_name = models.CharField(
        max_length=50,
        blank=True,
        null=True
    )

    # def __str__(self):
    #     return f"{self.customer_name}"

############################################################


class VerificationAddress(models.Model):
    """Verification address table"""

    fiType = (
        ('PV', 'PV'),
        ('BV', 'BV'),
        ('RV', 'RV'),
        ('PD', 'PD')
    )

    verification = models.ForeignKey(
        Verification,
        on_delete=models.CASCADE,
        related_name="addresses"
    )
    fi_type = models.CharField(
        choices=fiType,
        max_length=5,
    )
    adress = models.TextField()
    adress_district = models.ForeignKey(
        District,
        related_name="adress_district",
        on_delete=models.CASCADE
    )

    fi_date_time = models.DateTimeField()

    allocation_delay = models.CharField(max_length=100)

    class Meta:
        unique_together = ['verification', 'fi_type']
