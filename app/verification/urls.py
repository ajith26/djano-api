"""Verification Urls"""

from django.urls import path, include
from verification import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register('verification', views.AddVerificationViews)


urlpatterns = [
    path('', include(router.urls)),

]
