"""verification Serializers """

from rest_framework import serializers
from verification.models import Verification, VerificationAddress


class VerificationAddressSerializer(serializers.ModelSerializer):
    """Verification serializer"""

    class Meta:
        model = VerificationAddress
        # fields = '__all__'
        fields = [
            'fi_type',
            'adress',
            'fi_date_time',
            'allocation_delay',
            'adress_district'
        ]

##############################################################


class AddVerificationSerializer(serializers.ModelSerializer):
    addresses = VerificationAddressSerializer(
        many=True)

    class Meta:
        model = Verification
        fields = '__all__'

    def create(self, validated_data):
        print(validated_data)
        print("__________________________")
        addresses = validated_data.pop('addresses')
        verification = Verification.objects.create(**validated_data)
        for address in addresses:
            addresses = VerificationAddress.objects.create(
                verification=verification, **address)
        return verification

    def update(self, instance, validated_data):
        addresses = validated_data.pop('addresses')

        verificationInstance = Verification.objects.get(
            id=instance.id)

        for attr, value in validated_data.items():
            setattr(verificationInstance, attr, value)
        verificationInstance.save()

        for address in addresses:
            addresses = VerificationAddress.objects.create(
                verification=verificationInstance, **address)

        return verificationInstance




