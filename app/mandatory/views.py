"""Mandatory APIs"""

from rest_framework.viewsets import ModelViewSet
from .serializers import MandatorySerializer
from mandatory.models import Mandatory
from core.permissions import IsSuperAdmin


class MandatoryViews(ModelViewSet):
    serializer_class = MandatorySerializer
    queryset = Mandatory.objects.all()
    permission_classes = {IsSuperAdmin}
