from django.apps import AppConfig


class MandatoryConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mandatory'
