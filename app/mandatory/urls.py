"""Mandatory apis"""

from django.urls import path, include
from mandatory import views
from rest_framework import routers

router = routers.DefaultRouter()

router.register('mandatory-field', views.MandatoryViews)

urlpatterns = [
    path("", include(router.urls)),

]
