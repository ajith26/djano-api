"""Serializer for Mandatory"""

from rest_framework import serializers
from mandatory.models import Mandatory


class MandatorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Mandatory
        fields = '__all__'

    def create(self, validated_data):
        return Mandatory.objects.all().create(**validated_data)
