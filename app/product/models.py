"""Product Model"""

from django.db import models
from billinglocation.models import BillingLocation
from user.models import UserProfile
from django.db import IntegrityError
from django.core.exceptions import ValidationError
from rest_framework import serializers


class Product(models.Model):
    product_name = models.CharField(max_length=100, blank=True, null=True)
    product_code = models.CharField(max_length=50, blank=True, null=True)
    status = models.BooleanField(default=True)
    fi_service_start_time = models.DateTimeField(blank=True, null=True)
    fi_service_end_time = models.DateTimeField(blank=True, null=True)
    cut_off_time = models.DateTimeField(blank=True, null=True)
    local_distance = models.FloatField(blank=True, null=True)
    ocl_distance = models.FloatField(blank=True, null=True)
    ogl_distance = models.FloatField(blank=True, null=True)
    ogl_distance_1 = models.FloatField(blank=True, null=True)
    ogl_distance_2 = models.FloatField(blank=True, null=True)
    ogl_distance_3 = models.FloatField(blank=True, null=True)

    local_rate = models.DecimalField(
        max_digits=6, decimal_places=2, blank=True, null=True)
    ocl_rate = models.DecimalField(
        max_digits=6, decimal_places=2, blank=True, null=True)
    ogl_rate = models.DecimalField(
        max_digits=6, decimal_places=2, blank=True, null=True)
    ogl_rate_1 = models.DecimalField(
        max_digits=6, decimal_places=2, blank=True, null=True)
    ogl_rate_2 = models.DecimalField(
        max_digits=6, decimal_places=2, blank=True, null=True)
    ogl_rate_3 = models.DecimalField(
        max_digits=6, decimal_places=2, blank=True, null=True)

    billing_location = models.ForeignKey(
        BillingLocation, on_delete=models.CASCADE)
    vendor = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    vendor_code = models.CharField(
        max_length=100, null=True, blank=True, unique=True)
    credit_card_manager = models.CharField(
        max_length=100, null=True, blank=True)
    credit_card_manager_phone = models.CharField(
        max_length=100, null=True, blank=True)
    credit_card_manager_email = models.CharField(
        max_length=100, null=True, blank=True)

    def save(self, *args, **kwargs):
        self.product_code = self.product_code.upper()
        self.vendor_code = self.vendor.employee_code.upper()
        self.vendor_code += self.product_code.upper()
        self.vendor_code += self.billing_location.short_code.upper()
        if self.__class__.objects.filter(
                vendor_code=self.vendor_code).exists():
            raise serializers.ValidationError("Vendor code already exists")
        super(Product, self).save(*args, **kwargs)
