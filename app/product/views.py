"""Product APIs"""
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework import serializers
from rest_framework import status
from .serializers import ProductSerializer
from product.models import Product
from core.permissions import IsSuperAdmin


class ProductViews(ModelViewSet):
    serializer_class = ProductSerializer
    permission_classes = {IsSuperAdmin}
    queryset = Product.objects.all()

    # def create(self, request, *args, **kwargs):
    #     serializer = ProductSerializer(data=request.data)
    #     try:
    #         serializer.is_valid(raise_exception=True)
    #     except serializer.ValidationError:
    #         return Response(
    #             serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    #     else:
    #         return Response(
    #             serializer.data, status=status.HTTP_200_OK)

