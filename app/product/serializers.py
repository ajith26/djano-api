"""Serializer for Products"""

from rest_framework import serializers
from product.models import Product
# from rest_framework.exceptions import AuthenticationFailed
# from rest_framework.validators import UniqueValidator


class ProductSerializer(serializers.ModelSerializer):
    # vendor_code = serializers.CharField(
    #     max_length=100,
    #     validators=[UniqueValidator(queryset=Product.objects.all())]
    # )

    class Meta:
        model = Product
        fields = '__all__'

    def create(self, validated_data):
        return Product.objects.all().create(**validated_data)
