# Generated by Django 3.2.16 on 2022-11-29 07:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0002_product_vendor_code'),
    ]

    operations = [
        migrations.RenameField(
            model_name='product',
            old_name='vendor_code',
            new_name='vendor',
        ),
    ]
