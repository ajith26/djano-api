"""Product URLs"""

from django.urls import path, include
from product import views
from rest_framework import routers

router = routers.DefaultRouter()

router.register('create-product', views.ProductViews)

urlpatterns = [
    path("", include(router.urls)),

]
