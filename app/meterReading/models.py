"""Meter Reading"""

from django.db import models
from io import BytesIO
from PIL import Image
from django.core.files import File


def compress_image(image):
    im = Image.open(image)
    if im.mode != 'RGB':
        im = im.convert('RGB')

    im_io = BytesIO()
    im.save(im_io, 'webp', quality=70, optimize=True)
    new_image = File(im_io, name=image.name)

    return new_image


class MeterReading(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True)
    vehicle_km_reading = models.CharField(max_length=50)
    meter_reading_image = models.ImageField(null=True, blank=True)
    other_info = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.meter_reading_image:
            imageObj = self.meter_reading_image

            filename = imageObj.name.split('.')
            imageObj.name = filename[0]+'.webp'

            if imageObj.size > 0.3*1024*1024:
                self.avatar = compress_image(imageObj)
                super(MeterReading, self).save(*args, **kwargs)

