"""Meter Reading apis"""

from django.urls import path, include
from meterReading import views
from django.conf import settings
from django.conf.urls.static import static
from rest_framework import routers

router = routers.DefaultRouter()

router.register(
    'meter-reading', views.MeterReadingViews)

urlpatterns = [
    path("", include(router.urls)),

]

urlpatterns += static(
    settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    