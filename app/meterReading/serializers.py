"""Serializer for Meter Reading"""

from rest_framework import serializers
from meterReading.models import MeterReading


class MeterReadingSerializer(serializers.ModelSerializer):
    class Meta:
        model = MeterReading
        fields = ['name', 'vehicle_km_reading', 'meter_reading_image']

    def create(self, validated_data):
        return MeterReading.objects.all().create(
            **validated_data)
