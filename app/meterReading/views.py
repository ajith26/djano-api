"""Meter Reading APIs"""

from rest_framework.viewsets import ModelViewSet
from .serializers import MeterReadingSerializer
from meterReading.models import MeterReading
from core.permissions import IsSuperAdmin


class MeterReadingViews(ModelViewSet):
    serializer_class = MeterReadingSerializer
    queryset = MeterReading.objects.all()
    permission_classes = {IsSuperAdmin}
