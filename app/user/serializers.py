"""User serializers"""
from rest_framework import serializers

from user.models import User, UserProfile
from district.models import District

from django.contrib import auth
from rest_framework.exceptions import AuthenticationFailed
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode


class UserSerializer(serializers.ModelSerializer):

    token = serializers.CharField(max_length=255, read_only=True)

    class Meta:
        model = User
        fields = ['username', 'password', 'email',
                  'phonenumber', 'user_type', 'token']
# ###################################################################################


class DistrictSerializer(serializers.ModelSerializer):
    class Meta:
        model = District
        fields = ['name']

############################################################################


"""user profile create"""


class UserProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = UserProfile
        fields = '__all__'

    def create_or_get_userInstance(self, user):
        """creating new user"""

        username = user.get('username', '')
        password = user.get('password', '')
        if User.objects.filter(username__iexact=username).exists():
            raise serializers.ValidationError("Username already exist")
        if User.objects.filter(password__iexact=password).exists():
            raise serializers.ValidationError("Password already exist")

        employees = ['GroupLeader', 'ProductCoordinator',
                     'TeamMember', 'DistrictCoordinator',
                     'ExecutiveOfficeAdmin']
        print(user)
        if user['user_type'] == "Admin":
            return User.objects.create_superuser(**user)
        elif user['user_type'] in employees:
            return User.objects.create_employeeuser(**user)
        elif user['user_type'] == "FieldAgent":
            return User.objects.create_fieldagent(**user)
        elif user['user_type'] == "Vendor":
            return User.objects.create_vendor(**user)

    def create(self, validated_data):
        user = validated_data.pop('user', None)
        user_districts = validated_data.pop('user_districts', [])
        # get or create user instance
        userinstance = self.create_or_get_userInstance(user)
        validated_data['user'] = userinstance
        userprofileinstance = UserProfile.objects.create(**validated_data)
        # Adding districts
        for eachdistrict in user_districts:
            print(eachdistrict.name)
            userprofileinstance.user_districts.add(eachdistrict)

        return userprofileinstance

###############################################################


"""Edit user profile"""


class EditUserProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:

        model = UserProfile
        fields = ['user', 'employee_name', 'employee_code',
                  'account_holder', 'address', 'mac_id', 'salary',
                  'cpv_rate', 'pd_rate', 'bank', 'ifsc_code',
                  'account_number', 'date_of_birth', 'blood_group',
                  'description', 'user_districts']

    def update_user(self, user, instance):
        userinstance = User.objects.get(id=instance.user.id)
        for attr, value in user.items():
            setattr(userinstance, attr, value)
        userinstance.save()
        return userinstance

    def update(self, instance, validated_data):
        user = validated_data.pop('user', None)
        user_districts = validated_data.pop('user_districts', [])

        if user is not None:
            self.update_user(user, instance)

        for attr, value in validated_data.items():
            print("**: ", attr, value)
            setattr(instance, attr, value)
        instance.save()
        userprofileinstance = UserProfile.objects.get(id=instance.id)
        userprofileinstance.user_districts.set(user_districts)

        return userprofileinstance


##########################################################################

"""For user login"""


class LoginSerializer(serializers.Serializer):

    username = serializers.CharField(max_length=50)
    password = serializers.CharField(max_length=50, write_only=True)
    email = serializers.EmailField(read_only=True)
    user_type = serializers.CharField(max_length=100, read_only=True)
    access_token = serializers.CharField(max_length=100, read_only=True)
    refresh_token = serializers.CharField(max_length=100, read_only=True)

    class Meta:

        model = User
        fields = '__all__'

    def validate(self, validated_data):
        username = validated_data.get('username')
        password = validated_data.get('password')

        user = auth.authenticate(username=username, password=password)

        resp = {
            'username': user.username,
            'password': 'xxx',
            'email': user.email,
            'user_type': user.user_type,
            'access_token': user.tokens['access'],
            'refresh_token': user.tokens['refresh'],
            }
        if not user:
            raise AuthenticationFailed(
                {'error': 'invalid username & password'})
        return resp

##############################################################################


"""forgot password : sent link to email"""


class ResetPasswordEmailRequestSerializer(serializers.Serializer):
    email_id = serializers.EmailField(min_length=2)
    redirect_url = serializers.CharField(max_length=500, required=False)

    class Meta:
        fields = ['email']

##############################################################################


"""set new password After clicking the link"""


class SetNewPasswordSerializer(serializers.Serializer):
    password = serializers.CharField(
        min_length=6, max_length=68, write_only=True)
    token = serializers.CharField(
        min_length=1, write_only=True)
    uidb64 = serializers.CharField(
        min_length=1, write_only=True)

    class Meta:
        fields = ['password', 'token', 'uidb64']

    def validate(self, attrs):
        try:
            password = attrs.get('password')
            token = attrs.get('token')
            uidb64 = attrs.get('uidb64')
            print("_________*********__________")
            print(password)
            print(token)
            print(uidb64)

            id = force_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(id=id)

            if user.check_password(password):
                raise AuthenticationFailed(
                    'New password must not be similar to old password.', 401)

            if not PasswordResetTokenGenerator().check_token(user, token):
                raise AuthenticationFailed('link expired', 401)
            user.set_password(password)
            user.save()
            return (user)
        except Exception as e:
            raise AuthenticationFailed({'error': e.detail}, 401)
        return super().validate(attrs)

####################################################################


"""reset password serializer"""


class ResetPasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)

    class Meta:

        model = User
        fields = ['new_password', 'old_password']

    def validate(self, validated_data):
        print("##################")
        print(validated_data)
        try:
            old_password = validated_data.get('old_password')
            new_password = validated_data.get('new_password')

            if old_password == new_password:
                raise AuthenticationFailed(
                    'New password must not be similar to old password.',
                      401
                      )

        except Exception as e:
            raise AuthenticationFailed({'error': e.detail}, 401)

        return validated_data
