"""user APIs"""

from django.urls import path, include
from rest_framework import routers
from . import views
from rest_framework_simplejwt import views as jwt_views

router = routers.DefaultRouter()
# router.register('create-user',views.CreateUser)
# router.register('edit-profile',views.EditUserProfile)


urlpatterns = [
    path('', include(router.urls)),
    path(
        'token/',
        jwt_views.TokenObtainPairView.as_view(),
        name='token_obtain_pair'
        ),
    path(
        'token/refresh/',
        jwt_views.TokenRefreshView.as_view(),
        name='token_refresh'
        ),
    path('create-user/', views.CreateUser.as_view(),
         name='create-user'),
    path('edit-profile/<int:pk>', views.EditUserProfile.as_view(),
         name='edit-profile'),
    path('login/', views.UserLogin.as_view(),
         name='login'),
    path('forgot-password/', views.ForgotPassword.as_view(),
         name='forgot-password'),
    path('change-password/', views.ChangePassword.as_view(),
         name="change-password"),
    path('reset-password/', views.ResetPassword.as_view(),
         name="reset-password"),
    path('password-reset/<uidb64>/<token>/',
         views.PasswordTokenCheckAPI.as_view(),
         name='password-reset-confirm'),
]
