"""User APIs """

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from django.contrib.auth.tokens import PasswordResetTokenGenerator
from .tasks import test_func
from core.permissions import IsSuperAdmin
from .utils import Util
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import (
                        smart_bytes, smart_str, DjangoUnicodeDecodeError)
from django.contrib.sites.shortcuts import get_current_site

from user.models import User, UserProfile

from django.urls import reverse
# from rest_framework_simplejwt.authentication import JWTAuthentication
from .serializers import (
    UserProfileSerializer,
    EditUserProfileSerializer,
    ResetPasswordSerializer,
    LoginSerializer,
    SetNewPasswordSerializer,
    ResetPasswordEmailRequestSerializer
     )

import os
from django.http import HttpResponsePermanentRedirect


class CustomRedirect(HttpResponsePermanentRedirect):

    allowed_schemes = [os.environ.get('APP_SCHEME'), 'http']

##################################################################


class CreateUser(generics.ListCreateAPIView):
    """create user"""

    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer
    permission_classes = {IsSuperAdmin}
########################################################################


class EditUserProfile(generics.UpdateAPIView):
    """Edit User"""

    queryset = UserProfile.objects.all()
    serializer_class = EditUserProfileSerializer
    permission_classes = {IsSuperAdmin}
#######################################################################


class UserLogin(APIView):
    """user login"""

    serializer_class = LoginSerializer

    def post(self, request):

        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response({
            'success': True,
            'status': status.HTTP_200_OK,
            'data': serializer.data,
            'message': 'Login successful',
            }, status=status.HTTP_200_OK)
#######################################################################


class ForgotPassword(APIView):
    """Forgot-Password : sent link to email"""

    serializer_class = ResetPasswordEmailRequestSerializer
    permission_classes = {IsAuthenticated}

    def post(self, request):

        self.serializer_class(data=request.data)
        email_id = request.data.get('email_id', '')

        if User.objects.filter(email__iexact=email_id).exists():
            user = User.objects.get(email__iexact=email_id)
            uidb64 = urlsafe_base64_encode(smart_bytes(user.id))
            token = PasswordResetTokenGenerator().make_token(user)
            print("########______TOKEN_______#######")
            print(token)
            print(uidb64)

            redirect_url = request.data.get('redirect_url', '')
            # getting domain
            current_site = get_current_site(
                request=request).domain
            # endpoint
            relativeLink = reverse(
                'password-reset-confirm',
                kwargs={'uidb64': uidb64, 'token': token})

            absurl = 'https://'+current_site + relativeLink
            greeting = 'Hello, \n Use link below to reset your password  \n'
            email_body = greeting + absurl+"?redirect_url="+redirect_url

            # test sending email via celery

            print("TRYING TO SEND EMAIL.....")
            r = test_func.apply_async()
            print(r)
            print("___SENT......")

            data = {
                'email_body': email_body,
                'to_email': user.email,
                'email_subject': 'Reset your passsword'
                }
            Util.send_email(data)
            return Response({
                'success': 'We have sent you a link to reset your password',
                'msg': email_body
                },
                status=status.HTTP_200_OK
                )
        else:
            status_code = status.HTTP_400_BAD_REQUEST
            response = {
                'success': 'False',
                'status code': status.HTTP_400_BAD_REQUEST,
                'message': 'Email does not exists'
                }
            return Response(response, status=status_code)


# """After clicking the sent link: redirect to password reset page"""
class PasswordTokenCheckAPI(generics.GenericAPIView):

    serializer_class = SetNewPasswordSerializer

    def get(self, request, uidb64, token):

        redirect_url = request.GET.get('redirect_url')
        try:
            id = smart_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(id=id)
            print(user)
            return Response(user.username + " reset password page")
        except DjangoUnicodeDecodeError as identifier:
            print(identifier)
            try:
                if not PasswordResetTokenGenerator().check_token(user):
                    return CustomRedirect(redirect_url+'?token_valid=False')
            except UnboundLocalError:
                return Response(
                    {'error': 'Token is not valid, please request a new one'},
                    status=status.HTTP_400_BAD_REQUEST
                     )

#########################################################################


class ChangePassword(generics.GenericAPIView):

    """change/reset password"""

    serializer_class = SetNewPasswordSerializer
    permission_classes = {IsAuthenticated}

    def patch(self, request):

        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(
            {'success': True, 'message': 'Password reset success'},
            status=status.HTTP_200_OK
             )

#######################################################################


class ResetPassword(generics.GenericAPIView):
    """Reset password"""

    serializer_class = ResetPasswordSerializer
    permission_classes = {IsAuthenticated}
    model = User

    def patch(self, request):

        serializer = self.serializer_class(
            data=request.data,
            context={"user": request.user}
            )
        serializer.is_valid(raise_exception=True)
        user = request.user
        user.set_password(serializer.data['new_password'])
        user.save()
        return Response(
            {'success': True, 'message': 'Password reset success'},
            status=status.HTTP_200_OK
             )
