from celery import shared_task
from django.core.mail import send_mail


@shared_task()
def test_func():

    print("_________hiiiiiiiiiiiiiiiiiiiiiii________")
    send_mail(
        'TESTing CELERY :)',
        'This is from celery.',
        'aimee@metrictreelabs.com',
        ['aimeemary15@gmail.com'],
        fail_silently=False,
    )

    return "DONE"
