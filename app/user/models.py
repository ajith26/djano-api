"""USER TABLE"""

from django.db import models
from datetime import datetime, timedelta
from django.conf import settings
from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin
)
from district.models import District
import jwt


class UserManager(BaseUserManager):
    """ manager for users."""

    def create_user(self, username, email, password, phonenumber):
        """ create save and return user"""
        if username is None:
            raise TypeError('Users should have a valid username!')
        if email is None:
            raise TypeError('Users should have a valid email!')
        if password is None:
            raise TypeError('Password should not be done!')
        if phonenumber is None:
            raise TypeError('phonenumber should not be more than 12 digits!')

        user = self.model(username=username,
                          email=self.normalize_email(email),
                          phonenumber=phonenumber)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, username, email, phonenumber,
                         user_type, password=None):
        """ create and return new super user"""
        user = self.model(username=username,
                          email=self.normalize_email(email),
                          phonenumber=phonenumber,
                          user_type=user_type)
        user.set_password(password)
        user.is_superuser = True
        user.is_staff = True
        user.is_active = True
        user.save()
        return user

    def create_employeeuser(self, username, email, user_type,
                            phonenumber, password=None):
        user = self.model(username=username,
                          email=self.normalize_email(email),
                          phonenumber=phonenumber, user_type=user_type)
        user.set_password(password)
        user.is_superuser = False
        user.is_staff = True
        user.is_active = True
        user.save()
        return user

    def create_vendor(self, username, email, phonenumber,
                      user_type, password=None):
        user = self.model(username=username,
                          email=self.normalize_email(email),
                          phonenumber=phonenumber, user_type=user_type)
        user.set_password(password)
        user.is_superuser = False
        user.user_type = "Vendor"
        user.is_staff = False
        user.is_active = True
        user.save()
        return user

    def create_fieldagent(self, username, email, phonenumber,
                          user_type, password=None):
        user = self.model(username=username,
                          email=self.normalize_email(email),
                          phonenumber=phonenumber,
                          user_type=user_type)
        user.set_password(password)
        user.is_superuser = False
        user.user_type = "FieldAgent"
        user.is_staff = False
        user.is_active = True
        user.save()
        return user


class User(AbstractBaseUser, PermissionsMixin):
    """ user in the system"""

    usertypes = (
        ('Admin', 'Admin'),
        ('Vendor', 'Vendor'),
        ('FieldAgent', 'FieldAgent'),
        ('GroupLeader', 'GroupLeader'),
        ('ProductCoordinator', 'ProductCoordinator'),
        ('TeamMember', 'TeamMember'),
        ('DistrictCoordinator', 'DistrictCoordinator'),
        ('ExecutiveOfficeAdmin', 'ExecutiveOfficeAdmin')
    )
    username = models.CharField(max_length=255, unique=True)
    email = models.EmailField(max_length=255, blank=True, null=True,
                              unique=True)
    phonenumber = models.CharField(max_length=12, blank=True, null=True,
                                   unique=True)
    user_type = models.CharField(choices=usertypes, max_length=100)

    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_superuser = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['user_type', 'email', 'phonenumber']

    objects = UserManager()

    @property
    def tokens(self):
        refresh = RefreshToken.for_user(self)
        return {
            "refresh": str(refresh),
            "access": str(refresh.access_token)
        }

    @property
    def token(self):
        return self._generate_jwt_token()

    def _generate_jwt_token(self):
        """
        Generates a JSON Web Token that stores this user's ID
        and has an expiry date set to 5 min.
        """
        dt = datetime.now() + timedelta(minutes=5)

        token = jwt.encode({
            'id': self.pk,
            'exp': int(dt.strftime('%s'))
        }, settings.SECRET_KEY, algorithm='HS256')

        return token


""" USER-PROFILE TABLE """


class UserProfile(models.Model):
    bloodgroup = (
                ('A+', 'A+'),
                ('A-', 'A-'),
                ('B+', 'B+'),
                ('B-', 'B-'),
                ('O+', 'O+'),
                ('O-', 'O-'),
                ('AB+', 'AB+'),
                ('AB-', 'AB-')
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE,
                                related_name='user', blank=True,
                                null=True)
    employee_name = models.CharField(max_length=100, blank=True,
                                     null=True)
    employee_code = models.CharField(max_length=100, blank=True, null=True,
                                     unique=True)
    account_holder = models.CharField(max_length=100, blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    mac_id = models.CharField(max_length=100, blank=True, null=True)
    salary = models.CharField(max_length=100, blank=True, null=True)
    cpv_rate = models.FloatField()
    pd_rate = models.FloatField()
    bank = models.CharField(max_length=100, blank=True, null=True)
    ifsc_code = models.CharField(max_length=100, blank=True, null=True)
    account_number = models.CharField(max_length=100, blank=True, null=True)
    date_of_birth = models.DateField(blank=True, null=True)
    blood_group = models.CharField(
        choices=bloodgroup, max_length=4, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    user_districts = models.ManyToManyField(District,
                                            related_name="user_districts")

    def save(self, *args, **kwargs):
        self.employee_code = self.employee_code.upper()
        super(UserProfile, self).save(*args, **kwargs)
