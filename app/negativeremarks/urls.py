"""Negative Remarks URLs"""

from django.urls import path, include
from negativeremarks import views
from rest_framework import routers

router = routers.DefaultRouter()

router.register('negative-remark', views.NegativeRemarksViews)

urlpatterns = [
    path("", include(router.urls)),

]
