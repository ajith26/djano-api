"""Serializer for NegativeRemarks"""

from rest_framework import serializers
from negativeremarks.models import NegativeRemarks


class NegativeRemarksSerializer(serializers.ModelSerializer):
    class Meta:
        model = NegativeRemarks
        fields = '__all__'

    def create(self, validated_data):
        return NegativeRemarks.objects.all().create(**validated_data)
