
"""NEGATIVE REMARKS"""

from django.db import models


class NegativeRemarks(models.Model):
    comment = models.CharField(max_length=100)
    description = models.TextField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
