"""NegativeRemarks APIs"""

from rest_framework.viewsets import ModelViewSet
from .serializers import NegativeRemarksSerializer
from negativeremarks.models import NegativeRemarks
from core.permissions import IsSuperAdmin


class NegativeRemarksViews(ModelViewSet):
    serializer_class = NegativeRemarksSerializer
    queryset = NegativeRemarks.objects.all()
    permission_classes = {IsSuperAdmin}
